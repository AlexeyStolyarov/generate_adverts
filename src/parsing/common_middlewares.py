from scrapy import signals
from random import choice


class ProxyMiddleware(object):
    def __init__(self, http_proxy):
        self.http_proxy = http_proxy

    @classmethod
    def from_crawler(cls, crawler):
        o = cls(http_proxy=crawler.settings.get('HTTP_PROXY'))
        crawler.signals.connect(o.spider_opened, signal=signals.spider_opened)
        return o

    def spider_opened(self, spider):
        self.http_proxy = getattr(spider, 'http_proxy', self.http_proxy)

    def process_request(self, request, spider):
        if self.http_proxy:
            request.meta['proxy'] = self.http_proxy


class RandomUserAgentMiddleware(object):
    def __init__(self, user_agents_pool):
        self.user_agents_pool = user_agents_pool

    @classmethod
    def from_crawler(cls, crawler):
        o = cls(user_agents_pool=crawler.settings.get('RANDOM_USER_AGENTS_POOL', []))
        crawler.signals.connect(o.spider_opened, signal=signals.spider_opened)
        return o

    def spider_opened(self, spider):
        self.user_agents_pool = getattr(spider, 'user_agents_pool', self.user_agents_pool)

    def process_request(self, request, spider):
        if self.user_agents_pool:
            request.headers.setdefault(b'User-Agent', choice(self.user_agents_pool))
