import json

from scrapy import Request
from ...base_spider import BaseSpider

from apps.adverts_v2.options import PRINTEREST

__all__ = ('PrinterestSpider', )


PAGES_LIMIT = 10


class PrinterestSpider(BaseSpider):
    name = 'printerest-spider'
    allowed_domains = ('pinterest.com', 'pinimg.com', )
    donor = PRINTEREST

    custom_settings = {
        'ITEM_PIPELINES': {
            'parsing.common_pipelines.MakeInitialChecks': 10,
            'parsing.common_pipelines.BuildCommonData': 20,
            'parsing.common_pipelines.ValidateAndSaveItem': 100,
            'parsing.common_pipelines.ProcessItemPhotos': 150,
        },
    }

    def build_request(self, bookmarks):
        return Request(
            url=self.start_urls[0] % (('"%s"' % bookmarks) if bookmarks else ''),
            callback=self.parse,
            dont_filter=True,
            headers={
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'Accept-Encoding': 'gzip, deflate, sdch, br',
                'Accept-Language': 'u-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
                'X-APP-VERSION': '012736c',
                'X-NEW-APP': '1',
                'X-Pinterest-AppState': 'X-Pinterest-AppState',
                'X-Requested-With': 'XMLHttpRequest'
            }
        )

    def start_requests(self):
        yield self.build_request(None)

    def parse_list(self, response):
        try:
            response_data = json.loads(response.text)
        except ValueError:
            self.logger.error('Bad response recieved')
            return

        try:
            pins = response_data['resource_response']['data']
        except KeyError as error:
            self.logger.error('Cant find {} key in json response'.format(error))
            return

        for pin_info in pins:
            item = super().parse_item(response)

            try:
                item['donor_url'] = 'https://ru.printerest.com/pin/{}'.format(pin_info['id'])
                item['image_urls'] = [pin_info['images']['orig']['url']]
            except KeyError as warning_msg:
                self.logger.warning('Collect info from json error: key {} not found'.format(warning_msg))
                continue
            else:
                yield item

        try:
            next_page_bookmarks = response_data['resource']['options']['bookmarks']
        except KeyError as error:
            self.logger.error('Next page search error: key {} not found'.format(error))
        else:
            if next_page_bookmarks:
                yield self.build_request(next_page_bookmarks[0])
