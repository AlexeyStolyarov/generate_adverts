(function($) {
    
    var function_wrapper = function(parameter, context) {
        return $.isFunction(parameter) ? parameter.apply(context, Array.prototype.slice.call(arguments, 2)) : parameter;
    };
    
    /*
    *   Открытие диалогового окна с картинкой для кропа
    */
    $.fn.cropdialog = function(options) {
        var settings = $.extend(true, {}, $.fn.cropdialog.defaults, options);
        return this.on(settings.event, settings.event_selector, function() {
            var $element = $(this),
                source_image = function_wrapper(settings.source_image, settings, $element),
                jcrop_relation_x = 1, 
                jcrop_relation_y = 1;
            
            if (!source_image) {
                return false;
            }
            

            var dialog = $('<div/>')
                    .attr('id', 'crop-dialog')
                    .addClass('preload')
                    .appendTo('body');
            
            dialog.dialog($.extend({}, settings.dialog_opts, {
                buttons: [{
                    text: "Отмена",
                    click: function() {
                        dialog.dialog("close");
                    }
                }, {
                    text: "OK",
                    click: function() {
                        // Получаем координаты с учетом масштабирования картинки (left, top, width, height)
                        var coords = dialog.jcrop_api.tellSelect();
                        var real_coords = [];
                        real_coords.push(Math.round(jcrop_relation_x * coords.x));
                        real_coords.push(Math.round(jcrop_relation_y * coords.y));
                        real_coords.push(Math.round(jcrop_relation_x * coords.w));
                        real_coords.push(Math.round(jcrop_relation_y * coords.h));

                        // callback
                        if (settings.dialog_ok.call(settings, $element, dialog, real_coords) === false) {
                            return false
                        }
                        
                        dialog.dialog("close");
                    }
                }],
                open: function() {
                    var img = new Image;
                    img.onload = function() {
                        var dialog_img = $('<img>').attr({
                            src: img.src,
                            width: img.width,
                            height: img.height
                        }).prependTo(
                            dialog.removeClass('preload')
                        );
                        
                        dialog_img.Jcrop({
                            keySupport: false,
                            bgOpacity: 0.3,
                            boxWidth: settings.image_box_size[0],
                            boxHeight: settings.image_box_size[1],
                            boundary: 0
                        }, function() {
                            dialog.jcrop_api = this;

                            // callback
                            settings.dialog_open.call(settings, dialog);
                            
                            jcrop_relation_x = img.width / dialog_img.width();
                            jcrop_relation_y = img.height / dialog_img.height();
                            
                            // Ограничения размеров
                            var min_size = function_wrapper(settings.min_size, settings, $element);
                            if (min_size && (min_size[0] || min_size[1])) {
                                var minSize = [
                                    Math.ceil(min_size[0] / jcrop_relation_x),
                                    Math.ceil(min_size[1] / jcrop_relation_y)
                                ];
                                this.setOptions({minSize: minSize});
                                this.setSelect([0,0].concat(minSize));
                            }
                            var max_size = function_wrapper(settings.max_size, settings, $element);
                            if (max_size && (max_size[0] || max_size[1])) {
                                var maxSize = [
                                    Math.floor(max_size[0] / jcrop_relation_x),
                                    Math.floor(max_size[1] / jcrop_relation_y)
                                ];
                                this.setOptions({maxSize: maxSize})
                            }
                            
                            // Аспекты
                            var aspect = function_wrapper(settings.aspect, settings, $element);
                            if (aspect) {
                                this.setOptions({aspectRatio: aspect});
                            }
                            
                            // Положение кропа
                            var crop_position = function_wrapper(settings.crop_position, settings, $element);
                            if (crop_position && (crop_position.length == 4)) {
                                crop_position[0] = crop_position[0] / jcrop_relation_x;
                                crop_position[1] = crop_position[1] / jcrop_relation_y;
                                crop_position[2] = crop_position[2] / jcrop_relation_x;
                                crop_position[3] = crop_position[3] / jcrop_relation_y;
                                this.setOptions({
                                    setSelect: [
                                        crop_position[0],
                                        crop_position[1],
                                        crop_position[0] + crop_position[2],
                                        crop_position[1] + crop_position[3]
                                    ]
                                })
                            }
                        });
                    };
                    img.src = source_image;
                },
                close: function() {
                    // callback
                    if (settings.dialog_cancel.call(settings, $element, dialog) === false) {
                        return false
                    }
                    if (dialog.jcrop_api) {
                        dialog.jcrop_api.destroy();
                        dialog.jcrop_api = null;
                    }
                    dialog.dialog("destroy").remove();
                }
            }));
            
            return false;
        })
    };
    
    $.fn.cropdialog.defaults = {
        /*
        *   Событие, при наступлении которого будет открыто диалоговое окно
        */
        event: 'click.cropdialog',
        
        /* 
        *   Селектор элементов, событие event которых будет открывать диалог
        */
        event_selector: undefined,
        
        /* 
        *   Функция или значение пути к картинке, которая будет использоваться как исходник для обрезки.
        *   Если вернет false - диалог не откроется.
        *   Агрументы: $element
        */
        source_image: $.noop,
        
        /* 
        *   Функция или значение минимального размера обрезки
        *   Агрументы: $element
        */
        min_size: $.noop,
        
        /* 
        *   Функция или значение максимального размера обрезки
        *   Агрументы: $element
        */
        max_size: $.noop,
        
        /* 
        *   Функция или значение фиксированных аспектов обрезки.
        *   Агрументы: $element
        */
        aspect: $.noop,
        
        /*
        *   Максимальный размер картинки в окне
        */
        image_box_size: [600, 500],
        
        /*
        *   Начальная позиция окна обрезки
        */
        crop_position: $.noop,
        
        /*
        *   Опции диалога
        */
        dialog_opts: {
            title: 'Обрезать изображение',
            width: 610,
            minHeight: 300,
            show: { effect: "fadeIn", duration: 100 },
            hide: { effect: "fadeOut", duration: 100 },
            modal: true,
            resizable: false,
            position: { my: "center top", at: "center top+40", of: window }
        },
        
        /*
        *   Callback открытия диалога и применения Jcrop.
        *   Агрументы: dialog
        */
        dialog_open: $.noop,
        
        /*
        *   Callback отмены обрезки.
        *   Если вернет false - окно не закроется.
        *   Агрументы: $element, dialog
        */
        dialog_cancel: $.noop,
        
        /*
        *   Callback применения обрезки.
        *   Если вернет false - окно не закроется.
        *   Агрументы: $element, dialog, coords
        */
        dialog_ok: $.noop
    };
    
})(jQuery);