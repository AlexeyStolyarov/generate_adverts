import os
from django import forms
from django.conf import settings
from django.forms.widgets import FileInput
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe

ADMIN_VARIATION_NAME = getattr(settings, 'STDIMAGE_ADMIN_VARIATION', 'admin_thumbnail')


class StdImageWidget(forms.ClearableFileInput):
    class Media:
        js = [
            'admin/js/jquery-namespace-rollback.js',
            'admin/js/jquery-ui.min.js',
            'admin/js/jquery.cookie.js',
            'admin/js/jquery.Jcrop.js',
            'admin/js/preview_reader.js',
            'admin/js/cropdialog.js',
            'stdimage/admin/js/stdimage.js',
        ]
        css = {
            'all': (
                'admin/css/jquery-ui/jquery-ui.min.css',
                'admin/css/jcrop/jquery.Jcrop.css',
                'admin/css/cropdialog/cropdialog.css',
                'stdimage/admin/css/stdimage.css',
            )
        }

    def __init__(self, *args, **kwargs):
        self.preview_size = kwargs.pop('preview_size', ())
        self.aspects = kwargs.pop('aspects')
        self.crop_area = kwargs.pop('crop_area')
        self.min_dimensions = kwargs.pop('min_dimensions')
        self.max_dimensions = kwargs.pop('max_dimensions')
        super().__init__(*args, **kwargs)
        
    def render(self, name, value, attrs=None):
        params, self.attrs = self.attrs, {}
        
        input_tag = super(FileInput, self).render(name, value, dict(attrs, **{
            'class': 'uploader',
        }))
        
        context = dict(
            name=name,
            input=input_tag,
            
            preview_size=self.preview_size,
            aspects='|'.join(self.aspects),
            crop_area=self.crop_area,
            min_dimensions=self.min_dimensions,
            max_dimensions=self.max_dimensions,
            admin_variation_name=ADMIN_VARIATION_NAME,
        )
        
        if value and hasattr(value, 'field'):
            context['value'] = value
            admin_variation = getattr(value, ADMIN_VARIATION_NAME, None)
            if admin_variation:
                context['random'] = '?_=%i' % os.path.getmtime(admin_variation.path)

        return mark_safe(render_to_string('stdimage/admin_widget.html', context))

    def value_from_datadict(self, data, files, name):
        return (
            super().value_from_datadict(data, files, name),
            data.get('%s-delete' % name, False),
            data.get('%s-croparea' % name, None)
        )
