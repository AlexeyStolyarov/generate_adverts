from django.contrib.auth.views import login as django_login, auth_logout
from django.http import Http404
from django.shortcuts import redirect

from apps.adverts_v2.models import Category
from apps.main.forms import CustomAuthenticationForm


def login(request):
    if request.user.is_authenticated():
        return redirect('main:index')
    return django_login(request, template_name='main/login.html', authentication_form=CustomAuthenticationForm)


def logout(request):
    auth_logout(request)
    return redirect('main:index')


def index(request):
    first_category = Category.objects.first()
    if first_category is None:
        raise Http404
    return redirect('adverts_v2:new-adverts', first_category.alias)
