
class BaseTextGenerationException(Exception):
    """ Base exception """


class SkipPhrase(BaseTextGenerationException):
    """ Skip phrase """


class PhraseTemplateError(BaseTextGenerationException):
    """ Error phrase template formatting """
