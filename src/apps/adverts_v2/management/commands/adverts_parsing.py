import os

from django.core.management import BaseCommand
from scrapy.crawler import CrawlerProcess, CrawlerRunner
from scrapy.utils.project import get_project_settings
from twisted.internet import reactor, defer
from scrapy.utils.log import configure_logging

from apps.adverts_v2.models import Category


class Command(BaseCommand):
    help = 'Парсинг объявлений для всех категорий'

    def add_arguments(self, parser):
        parser.add_argument('--category', nargs='?', type=str)

    def handle(self, *args, **options):
        os.environ.setdefault('SCRAPY_SETTINGS_MODULE', 'parsing.settings')
        scrapy_runner = CrawlerRunner(settings=get_project_settings())

        queryset = Category.objects.all()
        if options['category']:
            queryset = queryset.filter(alias=options['category'])

        configure_logging()

        @defer.inlineCallbacks
        def crawl():
            for category in queryset:
                for parsing_settings in category.categoryparsingsettings_set.filter(is_active=True):
                    yield scrapy_runner.crawl(
                        parsing_settings.parser_name,
                        category=category.alias,
                        city=parsing_settings.city,
                        url=parsing_settings.parser_url,
                        pages_limit=parsing_settings.parser_pages_limit,
                        extra_context={
                            'PRICE_RANGE': range(
                                parsing_settings.price_from,
                                parsing_settings.price_to,
                                parsing_settings.price_step
                            )
                        }
                    )
            reactor.stop()

        crawl()
        reactor.run()
