from django import forms
from form_utils.forms import BetterModelForm


class CountForm(forms.Form):
    count = forms.IntegerField(
        label='Количество объявлений',
        max_value=100,
        min_value=1,
        initial=10,
        widget=forms.NumberInput(attrs={'class': 'form-control'})
    )


class BaseAdvertForm(BetterModelForm):
    class Meta:
        exclude = ('id', 'category', 'donor', 'donor_url', 'status', 'visible')
        widgets = {
            'title': forms.TextInput,
            'description_original': forms.Textarea(attrs={'readonly': 'readonly'}),
            'title_original': forms.TextInput(attrs={'readonly': 'readonly'})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields.values():
            if isinstance(field.widget, (forms.TextInput, forms.Textarea, forms.Select)):
                field.widget.attrs.update({'class': 'form-control'})
            elif isinstance(field.widget, forms.CheckboxInput):
                field.is_checkbox = True


FIELDSETS = {
    'default': (
        ('address', {
            'legend': 'Адрес',
            'fields': ('city', 'district', 'metro')
        }),
        ('common', {
            'legend': 'Основные данные',
            'fields': ('title_original', 'title', 'description_original', 'description', 'price')
        })
    ),
    'apartmentadvert': (
        ('address', {
            'legend': 'Адрес',
            'fields': ('city', 'district', 'metro', 'street', 'house_number')
        }),
        ('common', {
            'legend': 'Основные данные',
            'fields': ('title_original', 'title', 'description_original', 'description', 'price', 'rooms_number',
                       'floor', 'floors_total', 'area_living', 'area_total', 'area_kitchen', 'ceiling_height',
                       'beds_number', 'condition', 'apartment_type')
        }),
        ('address', {
            'legend': 'Удобства',
            'fields': ('has_furniture', 'has_kitchen', 'has_refrigerator', 'has_washing_machine',
                       'has_conditioner', 'has_tv', 'has_internet')
        }),
    ),
    'cottageadvert': (
        ('address', {
            'legend': 'Адрес',
            'fields': ('city', 'district', 'metro')
        }),
        ('common', {
            'legend': 'Основные данные',
            'fields': ('title_original', 'title', 'description_original', 'description', 'price', 'area_house',
                       'area_homestead', 'floors_house', 'wall_material')
        })
    )
}


def _get_fieldsets(model_name):
    return FIELDSETS.get(model_name) or FIELDSETS['default']


def modelform_fabric(model):
    meta = type('Meta', (BaseAdvertForm.Meta, ), {'model': model, 'fieldsets': _get_fieldsets(model._meta.model_name)})
    return type('AdvertChangeForm', (BaseAdvertForm, ), {'Meta': meta})
